<?php

ini_set('max_execution_time', 600);

include 'simple_html_dom.php';

	#CSV file header
$header = 'DISPOSITION;SIZE;PRICE;ADDRESS;STREET'.PHP_EOL;
file_put_contents('rent.csv',$header,FILE_APPEND);

	#set number of pages for your selection
$sites = 1;
	#set URL you want to scrap - it needs to end: &strana=
$url = '';

for ($i = 1; $i<=; $i++){
    $url = $url.$i;    
    $pages = file_get_html($url);
    if (!$pages){
        continue;
    }
    foreach ($pages->find('.result') as $flat){
        $title = $flat->find('h3.fn a');
        $title_text = $title['0']->plaintext;
        //finds position of first comma in the title
        $first_comma =  strpos($title_text,',');
        //finds second comma in the title
        $second_comma = strpos($title_text,',',$first_comma+1);
        //find position of unit ("m")
        $unit = strpos($title_text,'m',$second_comma+1);
        //substring dispozition
        $disposition = substr($title_text, $first_comma+1, $second_comma-$first_comma-1);
        //substring size
        $size = substr($title_text, $second_comma+1, $unit-$second_comma-1);
        
        $price_text = $flat->find('.price');
        
        if ($price_text){
            $price = $price_text['0']->plaintext;
        } else {
            $price = '';
        }
             
        $address_text = $flat->find('.address');
        $address = $address_text['0']->plaintext;
        $prague_pos = strpos($address,'Praha');
        $dash_pos = strpos($address,'-');
        if (!$dash_pos){
            $dash_pos = strlen($address);
        }
        $address = substr($address, $prague_pos, $dash_pos-$prague_pos-1);
        
        $street_text = $flat->find('.street-address');
        if ($street_text){
            $street = $street_text['0']->plaintext;        
        } else {
            $street = '';   
        }
                      
        $size = str_replace("&nbsp;", '', $size);
        $size = trim($size);
        
        $data = $disposition.';'.$size.';'.$price.';'.$address.';'.$street.PHP_EOL;
        
        file_put_contents('rent.csv',$data,FILE_APPEND);
        
    }
}   

?>